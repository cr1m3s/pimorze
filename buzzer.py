from gpiozero import TonalBuzzer
from time import sleep
from gpiozero.tones import Tone
import os

'''Pin number which we gonna use'''
buzzer = TonalBuzzer(26)


'''small hash for latin letter'''
eng_lib = {"A":".-", "B":"-...", "C":"-.-.", "D": "-..", "E": ".",\
           "F": "..-.", "G": "--.", "H":"....", "I": "..", "J": ".---",\
           "K": "-.-", "L": ".-..", "M": "--", "N": "-.", "O": "---",\
           "P": ".--.", "Q": "--.-", "R": ".-.", "S": "...", "T": "-",\
           "U": "..-", "V": "...-", "W": ".--", "X": "-..-", "Y": "-.--",\
           "Z": "--.."}
'''length of sound in seconds'''
sounds = {".": 0.1, "-": 0.3}

'''after each symbol pasuse for 0.1 second'''
def sound_prod(char):
    buzzer.play(800)
    sleep(sounds[char])
    buzzer.stop()
    sleep(0.1)

'''take an array of words, split them into letter, split letters in dots and dashes'''
def morze_trans(words):
    result = ""
    for word in words:
        for letter in word:
            result += letter + "["
            for char in eng_lib[letter]:
                sound_prod(char)
                result += char
            result+=']'
            os.system("clear")
            print(result)
            sleep(0.3)
        sleep(0.5)
        result += " "
    
    os.system("clear")
    print(result)

'''exiting program after presing ctrl+c'''
try:
    while True:
        user_input = input("Please enter your phrase: ")
        phrase = user_input.upper()
        replic = phrase.split(" ")
        morze_trans(replic)
        print()
except KeyboardInterrupt:
    print("Exiting...")

