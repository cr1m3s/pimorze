import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
led = 37
GPIO.setup(led, GPIO.OUT, initial = 0)
try:
    while(True):
        GPIO.output(led, GPIO.HIGH)
        print("ON")
        time.sleep(0.1)
        GPIO.output(led,GPIO.LOW)
        print("OFF")
        time.sleep(0.1)
        GPIO.output(led, GPIO.HIGH)
        print("ON")
        time.sleep(0.3)
        GPIO.output(led, GPIO.LOW)
        print("OFF")
        time.sleep(0.1)
except KeyboardInterrupt:
    GPIO.cleanup()
    print("Exiting..")
