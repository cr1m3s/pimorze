colorscheme tokyo-metro
set encoding=utf-8
let python_highlight_all=1
syntax on
set nocompatible              " required
filetype off                  " required
set number
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

let t:is_transparent = 0
function! Toggle_transparent()
	if t:is_transparent == 0
		hi Normal guibg=NONE ctermbg=NONE
		let t:is_transparent = 1
	else
		set background=dark
		let t:is_tranparent = 0
	endif
endfunction
nnoremap <C-t> : call Toggle_transparent()<CR>
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
"
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

"Python standarts
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

"almost all other
au BufNewFile,BufRead *.js, *.html, *.css, *.rb, *.sh
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2

"Folding
Plugin 'tmhedberg/SimpylFold'

"Flagging spaces
Plugin 'vim-scripts/indentpython.vim'

"Autocomplete
Plugin 'Valloric/YouCompleteMe'

"Spell checker
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'

"Tree
Plugin 'scrooloose/nerdtree'
map <C-n> :NERDTreeToggle<CR>
"CtrP
Plugin 'kien/ctrlp.vim'

"Powerline
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
