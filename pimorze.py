import os, time
from pygame import mixer
mixer.init()
dot_sound = mixer.Sound('dot.ogg')
dash_sound = mixer.Sound('dash.ogg')

eng_lib = {'A':".-", "B":"-...", "C":"-.-.", "D": "-..", "E": ".",\
           "F": "..-.", "G": "--.", "H":"....", "I": "..", "J": ".---",\
           "K": "-.-", "L": ".-..", "M": "--", "N": "-.", "O": "---",\
           "P": ".--.", "Q": "--.-", "R": ".-.", "S": "...", "T": "-",\
           "U": "..-", "V": "...-", "W": ".--", "X": "-..-", "Y": "-.--",\
           "Z": "--.."}

sounds = {".": 100, "-": 300}

def morze_trans(words):
    for word in words:
        for let in word:
            for ch in eng_lib[let]:
                print(ch)
                mixer.music.unpause()
                if ch == ".":
                    dot_sound.play()
                elif ch == "-":
                    dash_sound.play()
                mixer.music.pause()
                time.sleep(0.2)
            print(let)
            time.sleep(0.3)
        time.sleep(0.7)

while True :
    us_inp = input("Enter your phrase: ")
    print(us_inp)
    phrase = us_inp.upper()
    rep = phrase.split(" ")
    morze_trans(rep)
