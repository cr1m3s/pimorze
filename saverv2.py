#!/usr/bin/env python3
import os
import time
import argparse
import sys
import zipfile
from shutil import make_archive

source = ["/home/oleksii/code/byte_of", "/home/oleksii/code/ruby"]

parser = argparse.ArgumentParser(prog='Saver', description='Making backup of files.')

date_of_creation = '2019.03.11'
message = '%(prog)s 2.0 created by Oleksii ' + date_of_creation
d_help = 'Using user directories for backup.'

parser.add_argument('-v','--version', action='version', version=message) 
parser.add_argument('-d', '--destinations', action='store', help=d_help)
parser.parse_args()

args = parser.parse_args()

if args.destinations:
    source = [args.destinations]
else:
    print("Saving default directories")

target_dir = "/home/oleksii/backup"

today = target_dir + os.sep + time.strftime("%Y%m%d")

if not os.path.exists(today):
    os.mkdir(today)
    print("Directory {0} created".format(today))
else:
    print("Directory {0} already exists".format(today))

target = today + os.sep + time.strftime("%H%M%S")

for z_file in source:
    make_archive(target + z_file.split('/')[-1], 'zip', z_file)
    print(z_file + " successfully added")

'''
good realithation with importing sys and using default programs
zip_command = "zip -qr {0} {1}".format(target, ' '.join(source))

if os.system(zip_command) == 0:
    print("Success, copy made in: ", target)
else:
    print("Something gonna wrong")
'''
'''
Bad realithation with using built in libs doesn't work properly
zf = zipfile.ZipFile(target,mode ='w')
zf.write(source[0])
for file_n in source:
    zf.write(file_n)
    print("Saving {0} directory".format(file_n))
'''

